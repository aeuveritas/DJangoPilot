from django.urls import path, re_path
from blog.views import *

urlpatterns = [
    path('', PostLV.as_view(), name='index'),
    path('post/', PostLV.as_view(), name='post_list'),
    path('post/<slug:slug>/', PostDV.as_view(), name='post_detail'),
    path('archive/', PostAV.as_view(), name='post_archive'),
    path('<int:year>/', PostYAV.as_view(), name='post_year_archive'),
    path('<int:year>/<str:month>/', PostMAV.as_view(), name="post_month_archive"),
    path('<int:year>/<str:month>/<int:day>/', PostDAV.as_view(), name="post_day_archive"),
    path('today/', PostTAV.as_view(), name='post_today_archive'),
    path('tag/', TagTV.as_view(), name='tag_cloud'),
    #re_path(r'^tag/(?P<tag>[^/]+(?u))/$', PostTOL.as_view(), name='tagged_object_list'),
    path('tag/<str:tag>/', PostTOL.as_view(), name='tagged_object_list'),
    path('search/', SearchFormView.as_view(), name='search'),
    path('add/', PostCreateView.as_view(), name='add'),
    path('change/', PostChangeLV.as_view(), name='change'),
    path('<slug:slug>/update/', PostUpdateView.as_view(), name='update'),
    path('<slug:slug>/delete/', PostDeleteView.as_view(), name='delete'),
]
